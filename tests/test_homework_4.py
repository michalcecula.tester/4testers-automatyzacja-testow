import pytest

from src.homework_4_car_warranty import car_has_warranty


# new_car_low_mileage = {
#     "production_year": 2023,
#     "mileage": 30000
# }
#
#
# def test_new_car_with_low_mileage_has_warranty():
#     assert car_has_warranty(**new_car_low_mileage)
#
#
# def test_four_years_old_car_has_warranty():
#     assert car_has_warranty(2020, 40000)
#
#
# def test_new_car_with_low_mileage_has_warranty():
#     assert car_has_warranty(2023, 40000)
#
#
# def test_new_car_with_high_mileage_does_not_have_warranty():
#     assert not car_has_warranty(2023, 60001)


@pytest.mark.parametrize("year, mileage, expected", [
    (2023, 30000, True), (2023, 60001, False), (2018, 3000, False), (2018, 60001, False)
])
def test_car_warranty_status(year, mileage, expected):
    assert car_has_warranty(year, mileage) == expected
