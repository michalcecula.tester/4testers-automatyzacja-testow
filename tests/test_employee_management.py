from src import employee_management
from assertpy import assert_that


def test_generate_employee_dictionary():
    employee_dictionary = employee_management.generate_employee_dictionary()
    assert sorted(["email", "seniority_years", "female"]) == sorted(employee_dictionary.keys())


def test_two_generated_employee_dictionaries_are_different():
    employee_1 = employee_management.generate_employee_dictionary()
    employee_2 = employee_management.generate_employee_dictionary()
    assert assert_that(employee_1).is_not_equal_to(employee_2)


def test_generating_list_of_employees():
    employees = employee_management.generate_list_of_employees(10)
    assert len(employees) == 10


def test_filtering_list_of_employees_for_female():
    input_list = [
        {'email': '5f05091f-7080-407f-a931-301903f8619a@example.com', 'seniority_years': 24, 'female': False},
        {'email': '09d875ae-ec99-44d3-bc30-4bc90687c7fb@example.com', 'seniority_years': 33, 'female': False},
        {'email': '3a9af0e7-e30f-441c-929e-96a741e762f0@example.com', 'seniority_years': 37, 'female': True}
    ]
    female_employees = employee_management.filter_list_of_employees_by_gender(input_list)
    assert len(female_employees) == 1


def test_filtering_list_of_male_only_employees():
    input_list = [
        {'email': '5f05091f-7080-407f-a931-301903f8619a@example.com', 'seniority_years': 24, 'female': False},
        {'email': '09d875ae-ec99-44d3-bc30-4bc90687c7fb@example.com', 'seniority_years': 33, 'female': False},
        {'email': '3a9af0e7-e30f-441c-929e-96a741e762f0@example.com', 'seniority_years': 37, 'female': False}
    ]
    female_employees = employee_management.filter_list_of_employees_by_gender(input_list)
    assert len(female_employees) == 0

def test_filtering_list_of_female_only_employees():
    input_list = [
        {'email': '5f05091f-7080-407f-a931-301903f8619a@example.com', 'seniority_years': 24, 'female': True},
        {'email': '09d875ae-ec99-44d3-bc30-4bc90687c7fb@example.com', 'seniority_years': 33, 'female': True},
        {'email': '3a9af0e7-e30f-441c-929e-96a741e762f0@example.com', 'seniority_years': 37, 'female': True}
    ]
    female_employees = employee_management.filter_list_of_employees_by_gender(input_list)
    assert len(female_employees) == 3

def test_filtering_list_of_employees_for_male():
    input_list = [
        {'email': '5f05091f-7080-407f-a931-301903f8619a@example.com', 'seniority_years': 24, 'female': False},
        {'email': '09d875ae-ec99-44d3-bc30-4bc90687c7fb@example.com', 'seniority_years': 33, 'female': False},
        {'email': '3a9af0e7-e30f-441c-929e-96a741e762f0@example.com', 'seniority_years': 37, 'female': True},
        {'email': '3a9af0e7-e30f-441c-929e-96a741e362f0@example.com', 'seniority_years': 36, 'female': True}

    ]
    female_employees = employee_management.filter_list_of_employees_by_gender(input_list, female=False)
    assert len(female_employees) == 2

def test_filtering_list_of_employees_for_seniority():
    input_list = [
        {'email': '5f05091f-7080-407f-a931-301903f8619a@example.com', 'seniority_years': 2, 'female': False},
        {'email': '09d875ae-ec99-44d3-bc30-4bc90687c7fb@example.com', 'seniority_years': 3, 'female': False},
        {'email': '3a9af0e7-e30f-441c-929e-96a741e762f0@example.com', 'seniority_years': 8, 'female': True},
        {'email': '3a9af0e7-e30f-441c-929e-96a741e362f0@example.com', 'seniority_years': 9, 'female': True}

    ]
    senior_emails = employee_management.filter_list_of_employees_by_seniority_greater_than(input_list, 7)
    assert senior_emails == ['09d875ae-ec99-44d3-bc30-4bc90687c7fb@example.com', '5f05091f-7080-407f-a931-301903f8619a@example.com']