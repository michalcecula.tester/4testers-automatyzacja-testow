from homework_3 import check_if_animal_requires_vaccination


def test_cat_age_one_requires_vaccination():
    assert check_if_animal_requires_vaccination(1, "cat") == True


def test_dog_age_one_requires_vaccination():
    assert check_if_animal_requires_vaccination(1, "dog") == True


def test_dog_age_three_requires_vaccination():
    assert check_if_animal_requires_vaccination(3, "dog") == False
