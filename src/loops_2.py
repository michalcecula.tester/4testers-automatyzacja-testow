# PĘTLE MAPUJĄCE
def print_each_number_in_the_list_squared(input_list):
    for number in input_list:
        print(number ** 2)


def convert_celsius_to_farenheit(temp_in_celsius):
    return temp_in_celsius * 9 / 5 + 32


def map_temepratures_in_celsius_to_farenheit(list_in_celsius):
    farenheit_list = []
    for temp in list_in_celsius:
        temp_in_farenheit = convert_celsius_to_farenheit(temp)
        farenheit_list.append(temp_in_farenheit)
    return farenheit_list


# PĘTLE FILTRUJĄCE

def filter_grades_greater_or_equal_3(grades):
    acceptable_grades = []
    for grade in grades:
        if grade >= 3:
            acceptable_grades.append(grade)
    return acceptable_grades


if __name__ == '__main__':
    # numbers = [1, 5, 78, 90, 100]
    # print_each_number_in_the_list_squared(numbers)
    # list_in_celsius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]
    # print(map_temepratures_in_celsius_to_farenheit(list_in_celsius))
    grades = [5, 4, 5, 4, 1, 1, 0, 1]
    print(filter_grades_greater_or_equal_3(grades))
