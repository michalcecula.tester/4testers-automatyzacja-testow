class User:
    def __init__(self, email, password):
        self.__email = email
        self.__password = password
        self.__is_logged_in = False

    def login(self, password):
        if password == self.__password:
            self.__is_logged_in = True

    def is_logged_in(self):
        return self.__is_logged_in

    def __str__(self):
        return f"User {self.__email} login status: {self.__is_logged_in}"

if __name__ == '__main__':
    user_1 = User("foo@example", "123Password@@")
    print(user_1.is_logged_in())
    user_1.login("123Password2@")
    print(user_1)
    user_1.login("123Password@@")
    print(user_1)
