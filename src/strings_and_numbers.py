first_name = "Michał"
last_name = "Cecuła"
email = "michalcecula@gmail.com"

# F-string
bio = f"First name: {first_name}\nLast name: {last_name}\nEmail: {email}"
print(bio)

# Algebra

circle_radius = 6
area_of_a_circle_with_radius = 3.14 * circle_radius ** 2
print(f"area_of_a_circle_with_radius {circle_radius}:", area_of_a_circle_with_radius)

long_mathematical_expression = 10 - 9 * 2 - 11
print(long_mathematical_expression)
