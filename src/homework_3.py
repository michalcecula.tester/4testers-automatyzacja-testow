def validate_animal_data(age, kind):
    if age < 0:
        raise ValueError("Age must be greater than or equal to zero")
    elif kind not in ["dog", "cat"]:
        raise ValueError("Only dogs and cats are supported")


def is_animals_qualified_for_vaccination(age, kind):
    return kind == "dog" and (age - 1) % 2 == 0 or kind == "cat" and (age - 1) % 3 == 0


def check_if_animal_requires_vaccination(age, kind):
    validate_animal_data(age, kind)
    return is_animals_qualified_for_vaccination(age, kind)


if __name__ == '__main__':
    print(check_if_animal_requires_vaccination(4, "cat"))
