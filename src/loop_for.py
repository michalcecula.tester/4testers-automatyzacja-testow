def print_students_name_capitalized(list_of_students_first_names):
    for first_name in list_of_students_first_names:
        print(first_name.capitalize())


def print_first_ten_integers_squared():
    for number in range(0, 11):
        print(number ** 2)


if __name__ == '__main__':
    list_of_students = ["mate", "marek", "tosia", "niCKi", "stanley"]
    print_students_name_capitalized(list_of_students)

    print_first_ten_integers_squared()
