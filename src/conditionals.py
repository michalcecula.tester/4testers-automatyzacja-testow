def print_temperature_description(temperature_in_celsius):
    print(f"Temperature right now is {temperature_in_celsius} Celsius degrees")
    if temperature_in_celsius >= 29:
        print("It is hot!")
    elif temperature_in_celsius >= 15 and temperature_in_celsius < 29:
        print("It is quite OK")
    else:
        print("It is really cold!")


def is_person_an_adult(age):
    if age >= 18:
        return True
    else:
        return False

    # return True if age >= 18 else False


def check_if_normal_conditions(temp_in_celsius, pressure_in_hpa):
    if temp_in_celsius == 0 and pressure_in_hpa == 1013:
        return True
    else:
        return False


def results_on_exam(result_in_percent):
    if result_in_percent >= 90:
        return 5
    elif result_in_percent >= 75:
        return 4
    elif result_in_percent >= 50:
        return 3
    else:
        return 2


if __name__ == '__main__':
    print_temperature_description(19)
    age = 18
    print(is_person_an_adult(age))

    print(check_if_normal_conditions(0, 1013))
    print(check_if_normal_conditions(0, 1014))
    print(check_if_normal_conditions(1, 1013))
    print(check_if_normal_conditions(1, 1014))

    print(results_on_exam(49))
