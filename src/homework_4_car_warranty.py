from datetime import date


def car_has_warranty(production_year, mileage):
    age = date.today().year - production_year
    if age <= 5 and mileage <= 60000:
        return True
    else:
        return False


if __name__ == '__main__':
    print(car_has_warranty(2020, 70000))
