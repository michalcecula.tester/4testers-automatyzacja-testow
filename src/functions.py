def upper_word(word):
    return word.upper()


def add_two_numbers(a, b):
    return a + b


def calculate_cuboid_volume(a, b, h):
    return a * b * h


def convert_celsius_to_farenheit(temperature_in_celsius):
    return temperature_in_celsius * 9 / 5 + 32


def print_welcome_sentence(name, city):
    print(f"Witaj {name}! Miło Cię widzieć w naszym mieście {city}!")


if __name__ == '__main__':
    big_dog = upper_word("Burek")
    print(big_dog)

    print(upper_word("Cat"))

    equal = add_two_numbers("Michał", "Cecuła")
    print(equal)

    number_of_square = calculate_cuboid_volume(2, 4, 4)
    print(number_of_square)

    print(convert_celsius_to_farenheit(20))

    print(print_welcome_sentence("Beata", "Gdynia"))
