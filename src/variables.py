friends_name = "Piotrek"
friends_age = 31
friends_pet_amount = 1
has_driving_license = True
friendship_years = 11.5

print(
    "Name:", friends_name,
    "Age:", friends_age,
    "Friendship pet amount:", friends_pet_amount,
    "Has driving license:", has_driving_license,
    "Friendship years:", friendship_years,
    sep="\n",
)

name_surname = "Michał Cecuła"
email = "michalcecula@gmail.com"
phone_number = "722-056-222"

bio = f"Name: {name_surname}\nEmail: {email}\nPhone number: {phone_number}"
print(bio)

bio_docstring = f"""Name: {name_surname}
Email: {email}
Phone number: {phone_number}"""

print(bio_docstring)
