def describe_list_of_emails(emails):
    print("First element of the list is:", emails[0])
    print("Last element of the list is:", emails[-1])
    emails.append("blabla@gmail.com")
    print("Currently the list looks", emails)


def create_email_in_domain_4testers(first_name, last_name):
    return f"{first_name.lower()}.{last_name.lower()}@4testers.pl"


def create_email_for_user_in_domain(prefix, domain="gmail.com"):
    return f"{prefix}@{domain}"


def average_of_grades(list_of_grades):
    return sum(list_of_grades) / len(list_of_grades)


if __name__ == '__main__':
    describe_list_of_emails(["aaa@gmail.com", "bbb@gmail.com"])

    print(create_email_in_domain_4testers("Janusz", "Nowak"))
    print(create_email_in_domain_4testers("Barbara", "Kowalska"))

    list_of_grades = [5, 5, 3, 3, 1, 2]
    print(average_of_grades(list_of_grades))

    print(create_email_for_user_in_domain("michal"))

    print("pies", "kot", "żaba", sep=";")
