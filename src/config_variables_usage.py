from config import TRESHOLD


def send_slack_notification(value):
    print(f"Slack message: alert was triggered for value {value} after exceeding the treshold of {TRESHOLD}")


def trigger_allert(value):
    print(f"Alert triggered for value {value}")


def check_if_alert_should_be_trigerred(value):
    if value > TRESHOLD:
        trigger_allert(value)
        send_slack_notification(value)


if __name__ == '__main__':
    check_if_alert_should_be_trigerred(0.3)
    check_if_alert_should_be_trigerred(0.6)
