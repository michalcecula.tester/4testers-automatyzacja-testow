import random


def print_ten_numbers():
    for i in range(0, 10):
        print(i)


def print_even_numbers_in_range():
    for i in range(0, 121, 2):
        print(i)


def print_numbers_from_one_to_thirty_divisible_by_7():
    for i in range(1, 31):
        if i % 7 == 0:
            print(i)


def print_numbers_divisible_by_number_in_range(start, end, divider):
    for i in range(start, end):
        if i % divider == 0:
            print(i)


def print_random_numbers(n):
    for i in range(n):
        print(random.randint(100, 1000))


def generate_list_of_numbers_from_range():
    numbers = []
    for i in range(10):
        numbers.append((random.randint(1000, 5000)))
    return numbers


if __name__ == '__main__':
    # print_ten_numbers()
    # print_even_numbers_in_range()
    # print_numbers_from_one_to_thirty_divisible_by_7()
    # print_numbers_divisible_by_number_in_range(1, 31, 7)
    # print_random_numbers(5)
    print(generate_list_of_numbers_from_range())
