def print_description_of_gamer(gamer):
    source = gamer.get("source")
    print(f'The player {gamer_01["nick"]} is of type {gamer_01["type"]} and has {gamer_01["exp_points"]} EXP')
    print(f'The source of player is {source}')


if __name__ == '__main__':
    gamer_01 = {
        "nick": "gamer_54",
        "type": "warrior",
        "exp_points": 3000,
        "source": "Playstation"
    }
    gamer_02 = {
        "nick": "gamer_99",
        "type": "magician",
        "exp_points": 2000
    }
    print_description_of_gamer(gamer_01)
    print_description_of_gamer(gamer_02)