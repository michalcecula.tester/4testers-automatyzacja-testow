def get_person_data_dictionaries(email, phone, city, street):
    return {
        "contact": {
            "email": email,
            "phone": phone
        },
        "address": {
            "city": city,
            "street": street
        }
    }


if __name__ == '__main__':
    pet = {
        "name": "Burek",
        "kind": "Spaniel",
        "age": 10,
        "weight": 16,
        "is_male": True,
        "favourite_food": ["carrot", "cheese, cats"]
    }

print(pet)
print(pet["age"])
pet["age"] = 8
print(pet["age"])
pet["likes_swimming"] = False
print(pet)
del pet["likes_swimming"]
print(pet)
pet["favourite_food"].append("trash")
print(pet)

person_data = get_person_data_dictionaries("bla@gmail.com", "777-777-777", "Rzeszow", "Jagiellonska")
print(person_data)
