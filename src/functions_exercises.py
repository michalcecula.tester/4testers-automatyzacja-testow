def print_a_car_brand_name():
    print("Ford")


def print_given_number_multiplied_by_3(input_number):
    print(input_number * 3)


def calculate_area_of_a_circle(radius):
    return 3.1415 * radius ** 2


def calculate_area_of_triangle(bottom, height):
    return 0.5 * bottom * height


print_a_car_brand_name()
print_given_number_multiplied_by_3(3)
area_of_a_circle_with_radius_10 = calculate_area_of_a_circle(10)
print(area_of_a_circle_with_radius_10)
area_of_a_little_triangle = calculate_area_of_triangle(5, 5)
print(area_of_a_little_triangle)

# Tracking what is returned from a function by default
result_of_a_print_function = print_a_car_brand_name()
print(result_of_a_print_function)