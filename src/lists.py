def get_three_the_highest_numbers_in_the_list(list):
    sorted_list = sorted(list)
    return sorted_list[-3:]

    # sorted_list = sorted(list, reverse=True)
    # return sorted_list[0:3]


if __name__ == '__main__':
    list = [2, 5, 7, 10, 100, -5]
#     movies = ["Dune", "Blade Runner", "Solaris", "Lost Highway", "Star Wars"]
#
# print(len(movies))
# print(movies[0])
# print(movies[-1])
#
# movies.append("SSS")
# movies.insert(2, "404")
# movies.append("Five o'clock")
# print(movies)

print(get_three_the_highest_numbers_in_the_list(list))
