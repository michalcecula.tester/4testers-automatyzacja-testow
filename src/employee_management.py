import random
import uuid


def generate_employee_dictionary():
    return {
        "email": f"{uuid.uuid4()}@example.com",
        "seniority_years": random.randint(20, 60),
        "female": bool(random.randint(0, 1))
    }


def generate_list_of_employees(list_length):
    # employees = []
    # for i in range(list_length):
    #     employees.append(generate_employee_dictionary())
    # return employees
    employees = [generate_employee_dictionary() for i in range(list_length)]


def filter_list_of_employees_by_seniority_greater_than(employees, seniority_years):
    [employee["email"] for employee in employees if employee["seniority_years"] > seniority_years]


def filter_list_of_employees_by_gender(employees, female=True):
    # output = []
    # for employee in employees:
    #     if employee["female"] == female:
    #         output.append(employee)
    # return output

    [employee for employee in employees if employee["female"] == female]


if __name__ == '__main__':
    # employees = generate_list_of_employees(10)
    # print(employees)
    # senior_employees = filter_list_of_employees_by_seniority_greater_than_10(employees, seniority_years)
    # print(senior_employees)
    # female_employees = filter_list_of_employees_by_gender(employees)
    # print(female_employees)
    print(generate_list_of_employees(10))
