animal_kinds = ["dog", "fish", "cat"]

animals = [
    {
        "kind": "dog",
        "age": 2,
        "male": True
    },

    {
        "kind": "cat",
        "age": 3,
        "male": False
    },

    {
        "kind": "fish",
        "age": 1,
        "male": False
    }
]

print(animals)
print(len(animals))
last_animal = animals[-1]
last_animal_age = last_animal["age"]
print("Last animal age is equal to", last_animal_age)

print(animals[0]["kind"])
print(animals[1]["age"])

animals.append(
    {
        "kind": "bird",
        "age": 5,
        "male": True
    }
)

print(len(animals))