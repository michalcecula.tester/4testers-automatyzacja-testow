class Dog:
    def __init__(self, name, age):
        self.name = name
        self.age = age
        self.breed = "mixed"
        self.__is_hungry = True
        self.__needs_a_walk = False

    def get_name(self):
        return self.name

    def is_puppy(self):
        return self.age < 1

    def set_breed(self, new_breed):
        self.breed = new_breed

    def __str__(self):
        return f"My name is {self.name} and I am a {self.breed} dog."

    def feed(self):
        self.__is_hungry = False
        self.__needs_a_walk = True

    def is_hungry(self):
        return self.__is_hungry

if __name__ == '__main__':
    small_dog = Dog("Diana", 8)
    big_dog = Dog("Burek", 7)
    print(small_dog.get_name())
    print(big_dog.get_name())
    print(small_dog.name, small_dog.age)
    print(big_dog)
    small_dog.age = 3
    print(small_dog.age)
    small_dog.set_breed("Husky")
    print(small_dog)
    small_dog.feed()
    print(small_dog.is_hungry())
